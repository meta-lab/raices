# Propuesta: Analisis del desarrollo de raices
## Laboratorio de Investigacion de Ciencias Biologicas

El Objetivo del presente documento es presentar una propuesta de trabajo. Para luego compartir tanto el proceso de desarrollo, como sus resultados analiticos, y su futura aplicacion practica.

La propuesta consta de la participacion activa en un espacio de trabajo adaptable para los objectivos.

Se propone realizar un analisis comparativo del desarrollo de las raíces de diferentes especies, en diferentes medios nutrientes.
Medir y comparar la efectividad de diferentes [soluciones nutritivas](http://growlightsource.com/products/esc-treatment-concentrated-plant-root-conditioner/).

El siguiente documento, presenta un trabajo de referencia. El estudio se enfoca, dentro del desarrollo de las plantas, en analizar su respuesta al entorno.
Trabajando desde la perspectiva de la optica, empleando herramientas como la [Espectrofotometría](https://www.upo.es/depa/webdex/quimfis/docencia/quimbiotec/FQpractica4.pdf).

McCREE, K. J., 1972. "The action spectrum, absorptance and quantum yield of photosynthesis in crop plants." Agrie. MeteoroL, 9: 191-216.
Institute of Life Science and Biology Department, Texas A and M University, College Station, Texas (U.S.A.) (Received October 15, 1970)
http://www.inda-gro.com/IG/sites/default/files/pdf/ACTION-SPECTRUM-KJMCCREE.pdf

+info:
[CSA Group Seattle’s LED test & measurement laboratory](http://csagroupseattle.org/led-test-measurement-services/)
[Type C‐Horizontal Goniometer Report](http://csagroupseattle.org/wp-content/uploads/2015/04/SAMPLE-MG3020-type-c-gonometer.pdf)

### * Cualquier comentario sera mas que bienvenido! *